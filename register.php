<?php
  $pageTitle = "Registrazione";
?>
<!DOCTYPE html>
<html>
<?php include("inc/head.php"); ?>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">Crea un account</div>

  <div class="register-box-body">
    <form id="formRegister_1">
      <div class="form-group has-feedback">
        <label for="register--email">Indirizzo e-mail</label>
        <input type="email" class="form-control" placeholder="Email" id="register--email" />
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label for="register--nome">Utente</label>
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Nome" id="register--nome" />
          </div>
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Cognome" id="register--cognome" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--password">Crea password</label>
        <input type="password" class="form-control" id="register--password" placeholder="6-20 caratteri (sole lettere e numeri)">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label for="register--password-confirm">Conferma password</label>
        <input type="password" class="form-control" placeholder="Inserisci nuovamente la password" id="register--password-confirm" />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Autorizzo il trattamento dei miei dati personali ai sensi della legge 196/03 sulla privacy
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Conferma Dati Inseriti</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
