<?php
  $pageTitle = "Registrazione";
?>
<!DOCTYPE html>
<html>
<?php include("inc/head.php"); ?>
<body class="hold-transition register-page">
<div class="register-box" style="width:100%; max-width: 500px">
  <div class="register-logo">Obbligatorio per la sottomissione dei casi</div>

  <div class="register-box-body">
    <form id="formRegister_2">
      <div class="form-group has-feedback">
        <label for="register--intestazione">Intestazione Studio Dentistico/Lab. Odontotecnico</label>
        <input type="text" class="form-control" placeholder="Email" id="register--intestazione" />
      </div>
      <br />
      <div class="form-group has-feedback">
        <label for="register--dr-nome">Cognome & Nome (Dr./Odt.)</label>
        <div class="row">
          <div class="col-xs-12 col-md-4">
            <input type="text" class="form-control" placeholder="Dr/Odt." id="register--dr" />
          </div>
          <div class="col-xs-12 col-md-4">
            <input type="text" class="form-control" placeholder="Nome" id="register--dr-nome" />
          </div>
          <div class="col-xs-12 col-md-4">
            <input type="text" class="form-control" placeholder="Cognome" id="register--dr-cognome" />
          </div>
        </div>
      </div>
      <br />
      <div class="form-group has-feedback">
        <label for="register--sedeLegale-via">Sede Legale Studio Dentistico/Lab. Odontotecnico</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="Via & Numero" id="register--sedeLegale-via" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <div class="row">
          <div class="col-xs-12 col-md-4">
            <input type="text" class="form-control" placeholder="CAP" id="register--sedeLegale-CAP" maxlength="5" />
          </div>
          <div class="col-xs-12 col-md-8">
            <input type="text" class="form-control" placeholder="Città" id="register--sedeLegale-citta" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Provincia" id="register--sedeLegale-provincia" />
          </div>
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Stato" id="register--sedeLegale-stato" />
          </div>
        </div>
      </div>
      <br />
      <div class="form-group has-feedback">
        <label for="register--sedeOperativa-via">Sede Operativa Studio Dentistico/Lab. Odontotecnico</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="Via & Numero" id="register--sedeOperativa-via" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <div class="row">
          <div class="col-xs-12 col-md-4">
            <input type="text" class="form-control" placeholder="CAP" id="register--sedeOperativa-CAP" maxlength="5" />
          </div>
          <div class="col-xs-12 col-md-8">
            <input type="text" class="form-control" placeholder="Città" id="register--sedeOperativa-citta" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Provincia" id="register--sedeOperativa-provincia" />
          </div>
          <div class="col-xs-12 col-md-6">
            <input type="text" class="form-control" placeholder="Stato" id="register--sedeOperativa-stato" />
          </div>
        </div>
      </div>
      <br />
      <div class="form-group has-feedback">
        <label for="register--fattura-telefono">Telefono</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="+393331234567" id="register--fattura-telefono" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-cellulare">Cellulare</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="+393331234567" id="register--fattura-cellulare" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-email">Email</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="email" class="form-control" placeholder="latuaemail@iltuodominio.it" id="register--fattura-email" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-intestazione">Intestazione Fattura</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="Studio Dentistico Mario Rossi srl" id="register--fattura-intestazione" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-piva">Partita IVA</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="10243966795" id="register--fattura-piva" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-cf">Codice Fiscale</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="10243966795" id="register--fattura-cf" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-sdi">Codice SDI</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="10243966795" id="register--fattura-sdi" />
          </div>
        </div>
      </div>
      <div class="form-group has-feedback">
        <label for="register--fattura-pec">Indirizzo PEC</label>
        <div class="row">
          <div class="col-xs-12">
            <input type="email" class="form-control" placeholder="iltuoindirizzo@pec.it" id="register--fattura-pec" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Prosegui</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
